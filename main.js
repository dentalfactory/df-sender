const { app, BrowserWindow, shell, protocol } = require('electron')
const path = require('path')
const fs = require('fs');
const { ipcMain } = require('electron')
const { Client, MessageMedia } = require('whatsapp-web.js');
var firebase = require("firebase/app");
var admin = require('firebase-admin');
var serviceAccount = require("C:/Users/henrique.marques/Desktop/Electron/Electron/Electron/Electron/my-electron-app/testelogin-3fd10-firebase-adminsdk-2b3lw-136446465c.json");
require("firebase/auth");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount)
});
var firebaseConfig = {
  apiKey: "AIzaSyCitdq61NyXNe2mIlDBhvGXw1tAsvU6jtA",
  authDomain: "testelogin-3fd10.firebaseapp.com",
  projectId: "testelogin-3fd10",
  storageBucket: "testelogin-3fd10.appspot.com",
  messagingSenderId: "547822429402",
  appId: "1:547822429402:web:f7f89e400713e25a53b1e3"
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);
function createWindow() {
  const win = new BrowserWindow({
    width: 1200,
    height: 800,
    icon: '174879.png',
    webPreferences: {
      nodeIntegration: true,
      contextIsolation: false,
      nativeWindowOpen: true,
      enableRemoteModule: true,
      //preload: path.join(__dirname, 'preload.js')

    }
  })
  win.loadFile('./src/htmls/index.html')

  return win
}


  ipcMain.on('carregar', (event, arg) => {
    win.loadFile('./htmls/loading.html')
  })
  //win.removeMenu()
  let listamumeros = []
  let listanomes = []
  let tempomaximo = 0
  let tempominimo = 0
  let tempoentre = 0
  let mensagem = ''
  let arquivos = []
  ipcMain.on('Numbers', (event, arg) => {
    console.log(arg)
    listamumeros = arg
    fs.writeFileSync('contact.txt', JSON.stringify(arg));
  })
  ipcMain.on('Names', (event, arg) => {
    listanomes = arg
    var data = JSON.stringify(arg)
    data = data.replace(/\\n/g, '')
    console.log(data)
    fs.writeFileSync('names.txt', data);
  })
  ipcMain.on('Texto', (event, arg) => {
    mensagem = arg
    mensagem = mensagem.replace(/<br>/g, '\n')
    fs.writeFileSync('text.txt', arg);
  })

  ipcMain.on('Tempos', (event, arg) => {
    console.log(arg)
    tempomaximo = arg.max
    tempominimo = arg.mim
    tempoentre = arg.entre
    tempocontato = arg.contact
  })

  ipcMain.on('Comecar', (event, arg) => {
    comecarbot(listamumeros, listanomes, mensagem, tempominimo, tempomaximo, tempoentre, tempocontato, arquivos)
    arquivos = []
  })

  ipcMain.on('getPreviousList-send', (event, arg) => {
    let listnumber = fs.readFileSync('contact.txt', { encoding: 'utf8' })
    listnumber = listnumber.replace('[', '').replace(']', '').replace(/"/g, '')
    listnumber = listnumber.split(',')
    let listnames = fs.readFileSync('names.txt', { encoding: 'utf8' })
    listnames = listnames.replace('[', '').replace(']', '').replace(/"/g, '')
    listnames = listnames.split(',')
    console.log(listnames)
    var list = []
    for (i in listnumber) {
      var row = listnumber[i] + ';' + listnames[i]
      list.push(row)
    }
    console.log(list)
    event.reply('getPreviousList-reply', list)
  })

  ipcMain.on('saveNumber', (event, arg) => {
    fs.writeFileSync('contact.txt', JSON.stringify(arg));
  })
  ipcMain.on('saveNames', (event, arg) => {


    var data = JSON.stringify(arg)
    data = data.replace(/\\n/g, '')
    fs.writeFileSync('names.txt', data);
  })

  ipcMain.on('Arquivos', (event, arg) => {
    var arquivo = arg
    arquivo.forEach(element => {
      if (element.tipo.includes('image')) {
        console.log('uma imagem')
        element.url = element.url.replace(/^data:image\/\w+;base64,/, "");
      }
      else if (element.tipo.includes('video')) {
        element.url = element.url.replace(/^data:video\/\w+;base64,/, "");
        console.log('um video')
      }
      else if (element.tipo.includes('audio')) {
        element.url = element.url.replace(/^data:audio\/\w+;base64,/, "");
        console.log('um audio')
      }
      else if (element.tipo.includes('application')) {
        element.url = element.url.replace(/^data:application\/\w+;base64,/, "");
        console.log('um documento')
      }
      else {
        console.log('é um texto')
      }
    });
    arquivos = arquivo

  })
  ipcMain.on('testemsg', (event, arg) => {
    console.log(arg)
  })

  const comecarbot = async (contactlist, namaeslist, msg, temmin, temmax, tementre, tempocontato, arquivos) => {
    let listaenviados = []
    const max = parseInt(temmax)
    const min = parseInt(temmin)
    const tempentre = parseInt(tementre)
    const temp5contact = parseInt(tempocontato)
    console.log(temmax)
    console.log(temmin)
    const asyncForEach = async (array, callback) => {
      for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array)
      }
    }
    const waitFor = (ms) => new Promise(r => setTimeout(r, ms))

    const client = new Client({
      puppeteer: {
        headless: false,
        executablePath: 'C:/Program Files/Google/Chrome/Application/chrome.exe',
      }
    });


    client.initialize();

    client.on('qr', (qr) => {

      console.log('QR RECEIVED', qr);
    });
    let j = 0
    let i = 0
    client.on('ready', async () => {
      console.log('READY');
      let listamumeros = contactlist
      let listanomes = namaeslist

      await asyncForEach(listamumeros, async (num) => {
        console.log(listanomes[i])
        let tempo = tempoaleatorio(min, max)
        console.log("Esperando por:" + tempo)
        await waitFor(tempo);
        await mandamsg(num, listanomes[i], arquivos)
        i++
      })
      client.destroy();
      var infosfinais = {
        total: i,
        falhas: 0,
        sucesso: i,

      }

      win.webContents.send('EndProgramAlert', infosfinais)
    });

    client.on('disconnected', (reason) => {
      console.log('Client was logged out', reason);
    });
    client.on('message_create', (msg) => {
      // Fired on all message creations, including your own
      if (msg.fromMe) {
        win.webContents.send('Salvar')
      }
    });
    async function mandamsg(numero, nome, arquivos) {
      var result
      for (const element of arquivos) {

        if ((element.tipo.includes('image')) || (element.tipo.includes('video')) || (element.tipo.includes('application'))) {
          let arquivo = new MessageMedia(element.tipo, element.url, element.name)
          result = await client.sendMessage(numero + "@c.us", arquivo)
        }
        else if (element.tipo.includes('audio')) {
          let arquivo = new MessageMedia(element.tipo, element.url)
          result = await client.sendMessage(numero + "@c.us", arquivo, { sendAudioAsVoice: true })
        }

        else {
          if (element.text.includes('[Nome]')) {
            var msg = element.text.replace('[Nome]', nome).replace('\n', '')
            console.log(msg)
            result = await client.sendMessage(numero + "@c.us", msg)
          }
          else {
            result = await client.sendMessage(numero + "@c.us", element.text)
          }
        }
        listaenviados.push(numero)
        win.webContents.send('Enviados', numero)
        await waitFor(tempoaleatorio(tempentre - 1, tempentre + 1));

      };
      j++
      console.log(j)
      if (j === 5) {
        console.log('Deu 5 contatos esperando por', temp5contact)
        j = 0
        await waitFor(temp5contact)
      }
      console.log(result)
    }
  }

  function tempoaleatorio(min, max) {
    return (Math.floor(Math.random() * (max - min)) + min) * 1000
  }

  var win = null
  const gotTheLock = app.requestSingleInstanceLock()

  if (!gotTheLock) {
    app.quit()
  } else {
    app.on('second-instance', (event, commandLine, workingDirectory) => {
      win.webContents.send('Teste', commandLine[2])
      if (commandLine[2]) {
        var url = commandLine[2].split('/')
        var idToken = url[2]
        console.log('id: ', idToken)
  
        admin
          .auth()
          .verifyIdToken(idToken)
          .then((decodedToken) => {
            win.loadFile('./src/htmls/index.html')
            const uid = decodedToken.uid;
            console.log(decodedToken);
          })
          .catch((error) => {
            console.log(error)
          });
  
      }
      if (win.isMinimized()) win.restore()
      win.focus()
    })
  app.whenReady().then(() => {
    win = createWindow()

    app.on('activate', () => {
      if (BrowserWindow.getAllWindows().length === 0) {
        createWindow()
      }
    })
  })
  }
  app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
  })

  app.setAsDefaultProtocolClient('dfsender')

