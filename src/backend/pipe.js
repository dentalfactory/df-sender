const lib = require('pipedrive');
window.addEventListener("load", function () {
    createSelectOptions()
});

lib.Configuration.apiToken = 'ea7028b1b9f29e5520806cbb37bc6dec39d2517f';
async function getFiltros() {
    const type = 'deals'
    const result = await lib.FiltersController.getAllFilters(type)
    // console.log(result)
    return result.data
}

async function createSelectOptions() {
    var filtros = await getFiltros()
    var select = document.getElementById('filterspipe')
    for (var i = 0; i < filtros.length; i++) {
        var option = document.createElement("option");
        option.text = filtros[i].name;
        option.value = filtros[i].id;
        select.appendChild(option);
        
    }
}

async function getFiltrosNumbers() {
    let pessoas = []
    var select = document.getElementById('filterspipe')
    if (select.value) {
        var input = {}
        input['filterId'] = select.value;
        const result = await lib.DealsController.getAllDeals(input)
        //console.log(result)
        for (var i = 0; i < result.data.length; i++) {
            // console.log(result.data[i])
            let pessoa = {}
            let telefone = result.data[i].person_id.phone[0].value
            if (!telefone) {
                var deal = await lib.DealsController.getDetailsOfADeal(result.data[i].id)
                telefone = deal.data['b5fbcab78cc2769fdd1bd667a59bb1ebf0005c48']
            }
            telefone = telefone.replace(/\D/g, '');
            pessoa['telefone'] = formatTel(telefone)
            pessoa['nome'] = result.data[i].person_name
            pessoas.push(pessoa)
            
        }
        console.log(pessoas)
        generateTable(pessoas)
    } else {
        var dvCSV = document.getElementById("dvCSV");
        dvCSV.innerHTML = "";
    }
}

async function generateTable(persons) {
    
    var table = document.createElement("table");
    table.classList.add('table');
    table.classList.add('table-striped');
    table.classList.add('bg-white');
    table.id = "tabelamandar"
    for (var i = 0; i < persons.length; i++) {
        if (persons[i].telefone) {
            var row = table.insertRow(-1);
            var cel1 = row.insertCell(0)
            cel1.innerHTML = persons[i].telefone
            var cel2 = row.insertCell(1)
            cel2.innerHTML = persons[i].nome
        }
        else {
            console.log('nao tem numero de telefone')
        }
    }
    
    var dvCSV = document.getElementById("dvCSV");
    dvCSV.innerHTML = "";
    document.getElementById('enviados').innerHTML = "";
    dvCSV.appendChild(table);
    var header = table.createTHead();
    header.classList.add('thead-dark');
    var row = header.insertRow(0)
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    
    cell1.innerHTML = "Numero";
    cell2.innerHTML = "Nome";
    
}

function formatTel(phone) {
    let tamanho = phone.length
    let formatedphone = phone
    let telefone
    let ddd
    let cod
    let digito
    switch (tamanho) {
        case 13:
        telefone = phone.substring(5, 13)
        ddd = phone.substring(2, 4)
        cod = phone.substring(0, 2)
        digito = phone.substring(4, 5)
        if (!cod.includes('55')) {
            cod = '55'
        }
        if (!digito.includes('9')) {
            digito = '9'
        }
        if (ddd.includes('0')) {
            formatedphone = false
        }
        if (formatedphone) {
            formatedphone = cod + ddd + digito + telefone
        }
        break;
        case 12:
        telefone = phone.substring(tamanho - 8)
        ddd = phone.substring(2, 4)
        cod = phone.substring(0, 2)
        digito = '9'
        if (!cod.includes('55')) {
            cod = '55'
        }
        if (ddd.includes('0')) {
            formatedphone = false
        }
        if (formatedphone) {
            formatedphone = cod + ddd + digito + telefone
        }
        break;
        case 11:
        telefone = phone.substring(tamanho - 8)
        ddd = phone.substring(0, 2)
        cod = '55'
        digito = phone.substring(2, 3)
        if (!digito.includes('9')) {
            digito = '9'
        }
        if (ddd.includes('0')) {
            formatedphone = false
        }
        if (formatedphone) {
            formatedphone = cod + ddd + digito + telefone
        }
        break;
        case 10:
        telefone = phone.substring(tamanho - 8)
        ddd = phone.substring(0, 2)
        cod = '55'
        digito = '9'
        if (ddd.includes('0')) {
            formatedphone = false
        }
        if (formatedphone) {
            formatedphone = cod + ddd + digito + telefone
        }
        break;
        
        default:
        formatedphone = false
    }
    return formatedphone
}
