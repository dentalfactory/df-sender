const fs = require('fs');
const { Client, MessageMedia } = require('whatsapp-web.js');



//import {contact,content} from './config.js'


// const dir = 'C:/Users/henrique.marques/Desktop/Henrique/Dev/Robo/Imagem'

exports.comecarbot = async (contactlist, msg, temmin, temmax,tementre,tempocontato, arquivos) => {
  const max = parseInt(temmax)
  const min = parseInt(temmin)
  const tempentre = parseInt(tementre)
  const temp5contact = parseInt(tempocontato)
  const media = arquivos.length
  console.log(temmax)
  console.log(temmin)
  const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array)
    }
  }
  const waitFor = (ms) => new Promise(r => setTimeout(r, ms))

  const client = new Client({
    puppeteer: {
      headless: false,
      executablePath: 'C:/Program Files/Google/Chrome/Application/chrome.exe',
    }
  });


  client.initialize();

  client.on('qr', (qr) => {

    console.log('QR RECEIVED', qr);
  });
  let j = 0
  client.on('message_create', async() => {
      j ++
      console.log(j)
      if (j === 5 ){
        console.log('Deu 5 contatos esperando por',temp5contact)
        j= 0 
        await waitFor(temp5contact)
      }

  });

  client.on('ready', async () => {
    console.log('READY');
    let listamumeros = contactlist
    // let contactlist = getContact('./contactlist.txt')
    // contactlist = contactlist.split(/\r?\n/)


    await asyncForEach(listamumeros, async (num) => {
      let tempo = tempoaleatorio(min, max)
      console.log("Esperando por:" + tempo)
      await waitFor(tempo);
      await mandamsg(num, arquivos) 

    })
  });


  // const getContact = (path) => {
  //     const contact = fs.readFileSync(path, {encoding: 'utf-8'})
  //     return contact;
  //   }

  //   const getContent = (path) => {
  //     const content = fs.readFileSync(path, {encoding: 'utf-8'})
  //     return content;
  //   }




async function mandamsg(numero, arquivos) {

  for(const element of arquivos){

    if ((element.tipo.includes('image'))||(element.tipo.includes('video'))||(element.tipo.includes('application')))  {
      let arquivo = new MessageMedia(element.tipo, element.url,element.name)
      await client.sendMessage(numero + "@c.us", arquivo)
    }
   else if (element.tipo.includes('audio'))  {
      let arquivo = new MessageMedia(element.tipo, element.url)
      await client.sendMessage(numero + "@c.us", arquivo, { sendAudioAsVoice: true })
    }

    else {
      console.log("TEXTOO")
      await client.sendMessage(numero + "@c.us", element.text)
    }
    await waitFor(tempoaleatorio(tempentre-1, tempentre+1));
  };


}
}

function tempoaleatorio(min, max) {
  return (Math.floor(Math.random() * (max - min)) + min) * 1000
}
