var arquivos = []
function textedit(e) {

    e.target.style.display = "none"
    e.target.nextElementSibling.style.display = "block"
    var comment = e.target.parentElement.parentElement.previousElementSibling.firstElementChild.firstElementChild.children[0]
    var attr = comment.getAttributeNode("disabled");
    comment.removeAttributeNode(attr);
    comment.focus();
    comment.placeholder = 'Digite alguma coisa...'

}
function confirmedit(e) {
    e.target.style.display = "none"
    e.target.previousElementSibling.style.display = "block"
    var comment = e.target.parentElement.parentElement.previousElementSibling.firstElementChild.firstElementChild.children[0]
    comment.placeholder = 'Clique em "editar" para modificar a mensagem'
    var att = document.createAttribute("disabled");
    comment.setAttributeNode(att);
    var position = getposition(e.target.parentElement.parentElement.parentElement)
    var msg = {
        text: comment.value,
        tipo: 'text',
        position: position
    }
    if (position <= arquivos.length) {
        arquivos[position] = msg
        console.log('ENTROU AQUI')
    }
    else {
        arquivos[position] = msg

    }

}
document.getElementById("enviar").addEventListener('click', function () {
    if ((!arquivos.includes('undefined')) && (!arquivos.length == 0)) {
        console.log(!arquivos.includes('undefined'), !arquivos.length == 0)
        var temmim = document.getElementById('temmin').value
        var temmax = document.getElementById('temmax').value
        var temmsg = document.getElementById('entremsgtempo').value
        var temcontato = document.getElementById('tempocontatos').value
        var numerosarray = document.querySelectorAll('table tbody td:first-child')
        var nomesarray = document.querySelectorAll('table tbody td:nth-child(2)')
        var numeros = []
        var nomes = []
        var tempo = {
            max: temmax,
            mim: temmim,
            entre: temmsg,
            contact: temcontato
        }
        numerosarray.forEach(function (numero) {
            numeros.push(numero.innerHTML)
        })
        nomesarray.forEach(function (nome) {

            nomes.push(nome.innerHTML)
        })

        ipcRenderer.send('Numbers', numeros)

        ipcRenderer.send('Names', nomes)

        ipcRenderer.send('Tempos', tempo)

        ipcRenderer.send('Arquivos', arquivos)

        ipcRenderer.send('Comecar')



        var tableenviados = document.getElementById('enviados')
        var table = document.createElement("table");
        table.classList.add('table');
        table.classList.add('table-striped');
        table.classList.add('bg-white');
        table.id = 'tabelaenviaos'
        tableenviados.innerHTML = ''
        tableenviados.appendChild(table);
        header = table.createTHead();
        row = header.insertRow(0)
        var cell1 = row.insertCell(0);
        //var cell2 = row.insertCell(1);
        cell1.innerHTML = "Numero";
        //.innerHTML = "Nome";
    }
    else {
        alert("Existem campos faltando")
    }
})
function vermedias(e) {
    console.log(arquivos)
    // ipcRenderer.send('Arquivos', arquivos)
}
function dropHandler(ev) {
    ev.stopPropagation()
    document.getElementById('drop_zone').classList.remove('over')
    var div = ev.target.parentElement.parentElement
    // Impedir o comportamento padrão (impedir que o arquivo seja aberto)
    ev.preventDefault();
    if (ev.dataTransfer.items) {
        // Use a interface DataTransferItemList para acessar o (s) arquivo (s)
        for (var i = 0; i < ev.dataTransfer.items.length; i++) {
            // Se os itens soltos não forem arquivos, rejeite-os
            if (ev.dataTransfer.items[i].kind === 'file') {
                var file = ev.dataTransfer.items[i].getAsFile();
                ev.target.parentElement.parentElement.removeAttribute("style")


                if (file.type.includes('image')) {
                    // console.log('IMAGEM')
                    var img = document.createElement('img')
                    img.style.display = 'block'
                    img.style.marginLeft = 'auto'
                    img.style.marginRight = 'auto'
                    img.style.width = '25%'
                    var position = getposition(ev.target.parentElement.parentElement.parentElement)
                    var dataurl = getfiledata(file, img, position)
                    div.appendChild(img)


                }
                else if (file.type.includes('video')) {
                    var video = document.createElement('video')
                    var att = document.createAttribute("controls");
                    video.setAttributeNode(att);
                    video.style.display = 'block'
                    video.style.marginLeft = 'auto'
                    video.style.marginRight = 'auto'
                    video.style.width = '50%'
                    var position = getposition(ev.target.parentElement.parentElement.parentElement)
                    var dataurl = getfiledata(file, video, position)
                    div.appendChild(video)

                }
                else if (file.type.includes('audio')) {
                    console.log('um audio')
                    var audio = document.createElement('audio')
                    var att = document.createAttribute("controls");
                    audio.setAttributeNode(att);
                    audio.style.marginLeft = '80px'
                    var position = getposition(ev.target.parentElement.parentElement.parentElement)
                    var dataurl = getfiledata(file, audio, position)
                    div.appendChild(audio)
                }
                else if (file.type.includes('application')) {
                    var para = document.createElement("p");
                    var node = document.createTextNode("This is new.");
                    para.appendChild(node);
                    div.appendChild(para)
                    para.style.marginLeft = 'auto'
                    para.style.marginRight = 'auto'
                    var position = getposition(ev.target.parentElement.parentElement.parentElement)
                    var dataurl = getfiledata(file, audio, position)
                }

                ev.target.parentElement.style.display = 'none'
                console.log(ev.target.children[0])
                ev.target.children[0].innerText = 'Clique ou arraste os arquivos para cá'

            }
        }
    } else {
        // Use a interface DataTransfer para acessar o (s) arquivo (s)
        for (var i = 0; i < ev.dataTransfer.files.length; i++) {
            console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
            document.getElementById('drop_zone').innerText = file[i].name

        }
    }
}
var openFile = function (event) {
    var input = event.target;
    var file = input.files[0]
    var div = event.target.parentElement.parentElement.parentElement

    console.log(div)
    if (file.type.includes('image')) {
        // console.log('IMAGEM')
        var img = document.createElement('img')
        img.style.display = 'block'
        img.style.marginLeft = 'auto'
        img.style.marginRight = 'auto'
        img.style.width = '25%'
        var position = getposition(event.target.parentElement.parentElement.parentElement.parentElement)
        var dataurl = getfiledata(file, img, position)
        div.appendChild(img)


    }
    else if (file.type.includes('video')) {
        var video = document.createElement('video')
        var att = document.createAttribute("controls");
        video.setAttributeNode(att);
        video.style.display = 'block'
        video.style.marginLeft = 'auto'
        video.style.marginRight = 'auto'
        video.style.width = '50%'
        var position = getposition(event.target.parentElement.parentElement.parentElement.parentElement)
        var dataurl = getfiledata(file, video, position)
        div.appendChild(video)

    }
    else if (file.type.includes('audio')) {
        console.log('um audio')
        var audio = document.createElement('audio')
        var att = document.createAttribute("controls");
        audio.setAttributeNode(att);
        audio.style.marginLeft = '80px'
        var position = getposition(event.target.parentElement.parentElement.parentElement.parentElement)
        var dataurl = getfiledata(file, audio, position)
        div.appendChild(audio)
    }
    else if (file.type.includes('application')) {
        var para = document.createElement("p");
        var node = document.createTextNode("This is new.");
        para.appendChild(node);
        div.appendChild(para)
        para.style.marginLeft = 'auto'
        para.style.marginRight = 'auto'
        var position = getposition(event.target.parentElement.parentElement.parentElemen.parentElementt)
        var dataurl = getfiledata(file, audio, position)
    }

    event.target.parentElement.parentElement.style.display = 'none'
    console.log(event.target.parentElement.children[0])
    event.target.parentElement.children[0].innerText = 'Clique ou arraste os arquivos para cá'


};
function dragOverHandler(ev) {
    ev.preventDefault();
}
var dragSrcEl = null;

function handleDragStart(e) {
    e.target.style.opacity = '0.4';
    dragSrcEl = e.target;
    console.log('oi')
    console.log("opa")
    e.dataTransfer.effectAllowed = 'move';
    e.dataTransfer.setData('text/html', e.target.innerHTML);
    var dropzones = document.querySelectorAll('img,.drop_zone')
    dropzones.forEach(function (item) {
        item.style.pointerEvents = 'none'
    });
    var forms = document.querySelectorAll('#edittext')
    forms.forEach(function (item) {
        item.style.pointerEvents = 'none'
    });
    var media = document.querySelectorAll('#divdrop')
    media.forEach(function (item) {
        item.style.pointerEvents = 'none'
    });

}

function handleDragOver(e) {

    e.preventDefault();


    return false;
}

function handleDragEnter(e) {
    e.target.classList.add('over');
    e.target.children[0].innerText = "Solte os arquivos aqui"
}

function handleDragLeave(e) {
    e.target.classList.remove('over');
    e.target.children[0].innerText = "Clique ou Arraste os arquivos para cá"
}

function handleDrop(e) {
    console.log('oioi')
    if (e.stopPropagation) {
        e.stopPropagation(); // stops the browser from redirecting.
    }
    var positionSrc = getposition(dragSrcEl)
    var positiontarget = getposition(e.target.parentElement)

    if (positionSrc < positiontarget) {
        if (dragSrcEl != e.target) {
            dragSrcEl.parentElement.insertBefore(dragSrcEl, e.target.parentElement.nextElementSibling)
            console.log('POSIÇAO: ', positionSrc)
            var temparq = arquivos.splice(positionSrc, 1)
            arquivos.splice(positiontarget, 0, temparq[0])
        }
    }
    else {
        if (dragSrcEl != e.target) {
            dragSrcEl.parentElement.insertBefore(dragSrcEl, e.target.parentElement)
            var temparq = arquivos.splice(positionSrc, 1)
            arquivos.splice(positiontarget, 0, temparq[0])
        }
    }
    return false;
}

function handleDragEnd(e) {
    e.target.style.opacity = '1';
    var dropzones = document.querySelectorAll('.drop_zone')
    dropzones.forEach(function (item) {
        item.style.pointerEvents = 'auto'
    });
    var forms = document.querySelectorAll('#edittext')
    forms.forEach(function (item) {
        item.style.pointerEvents = 'auto'
    });
    var media = document.querySelectorAll('#divdrop')
    media.forEach(function (item) {
        item.style.pointerEvents = 'auto'
    });
    // items.forEach(function (item) {
    //     item.classList.remove('over');
    // });
}

let items = document.querySelectorAll('.box');
items.forEach(function (item) {

    item.addEventListener('dragstart', handleDragStart, false);
    item.addEventListener('dragenter', handleDragEnter, false);
    item.addEventListener('dragover', handleDragOver, false);
    item.addEventListener('dragleave', handleDragLeave, false);
    item.addEventListener('drop', handleDrop, false);
    item.addEventListener('dragend', handleDragEnd, false);
});

const electron = require('electron');
const ipcRenderer = electron.ipcRenderer;

function dropdown(e) {
    var sender = e.srcElement || e.target;
    //console.log(e.target)
    // document.getElementById('myDropdown').classList.toggle("show");
    e.srcElement.nextElementSibling.classList.toggle("show");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function (event) {

    if (!event.target.matches('.dropbtn')) {
        var dropdowns = document.getElementsByClassName("dropdown-content");
        var i;
        for (i = 0; i < dropdowns.length; i++) {
            var openDropdown = dropdowns[i];
            if (openDropdown.classList.contains('show')) {
                openDropdown.classList.remove('show');
            }
        }
    }
}
function Upload() {
    var fileUpload = document.getElementById("fileUpload");
    var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.csv|.txt)$/;
    if (regex.test(fileUpload.value.toLowerCase())) {
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var table = document.createElement("table");
                table.classList.add('table');
                table.classList.add('table-striped');
                table.classList.add('bg-white');
                table.id = "tabelamandar"
                var rows = e.target.result.split("\n");
                console.log(rows)
                for (var i = 0; i < rows.length; i++) {
                    var cells = rows[i].split(";");
                    if (cells.length >= 1) {
                        var row = table.insertRow(-1);
                        for (var j = 0; j < cells.length; j++) {
                            var cell = row.insertCell(-1);
                            cell.innerHTML = cells[j];
                        }
                    }
                }
                var dvCSV = document.getElementById("dvCSV");
                dvCSV.innerHTML = "";
                document.getElementById('enviados').innerHTML = "";
                dvCSV.appendChild(table);
                var header = table.createTHead();
                header.classList.add('thead-dark');
                var row = header.insertRow(0)
                var cell1 = row.insertCell(0);
                var cell2 = row.insertCell(1);

                cell1.innerHTML = "Numero";
                cell2.innerHTML = "Nome";
            }
            reader.readAsText(fileUpload.files[0]);
            var filename = fileUpload.value.split("\\").pop()
            document.getElementById("fileNameList").value = filename
            document.getElementsByClassName('clearList')[0].style.display = "flex"
            document.getElementsByClassName('image-preview-input-title p-1')[0].style.display = "none"
            document.getElementById('filterspipe').value=''
        } else {
            alert("This browser does not support HTML5.");
        }
    } else {
        alert("Please upload a valid CSV file.");
    }
}
function clearList() {
    document.getElementsByClassName('clearList')[0].style.display = "none"
    document.getElementsByClassName('image-preview-input-title p-1')[0].style.display = "flex"
    document.getElementById('dvCSV').innerHTML = ''
    document.getElementById('fileNameList').value = ''
    document.getElementById('fileUpload').value = ''
}
function getfiledata(file, img, position) {

    var reader = new FileReader();
    reader.onload = function () {
        var dataURL = reader.result;
        var imginfo = {
            'name': file.name,
            'url': dataURL,
            'tipo': file.type,
            'position': position
        }
        console.log(position)
        if (position <= arquivos.length) {
            arquivos[position] = imginfo
            console.log('ENTROU AQUI')
        }
        else {
            console.log('foioiooiio')
            arquivos[position] = imginfo
        }
        img.src = dataURL;


    };
    reader.readAsDataURL(file);

    return reader.result
}
document.getElementById("Addmedia").addEventListener('click', function () {
    var media = document.getElementById("templatemedia")
    var cln = media.content.cloneNode(true);
    //console.log(cln.firstElementChild.firstElementChild.firstElementChild.firstElementChild.innerText)
    document.getElementById("listamedias").appendChild(cln)

})

function teste(e) {
    var selectedicon = e.target.children[0].innerText
    console.log(selectedicon)
    //console.log(selectedicon)
    console.log('AAAAAAAAAAAAA', e.target.parentElement.parentElement.parentElement.nextElementSibling.firstElementChild.style.display = "flex")
    //console.log(e.target.parentElement.parentElement.parentElement.parentElement.children[2].children[2])
    switch (selectedicon) {
        case "image":
            e.target.parentElement.previousElementSibling.firstElementChild.innerHTML = "image"
            e.target.parentElement.parentElement.parentElement.nextElementSibling.firstElementChild.style.display = "none"
            e.target.parentElement.parentElement.parentElement.nextElementSibling.children[1].style.display = "flex"
            e.target.parentElement.parentElement.parentElement.nextElementSibling.nextElementSibling.firstElementChild.style.display = "none"
            if (e.target.parentElement.parentElement.parentElement.nextElementSibling.children[2]) e.target.parentElement.parentElement.parentElement.nextElementSibling.children[2].remove()
            e.target.parentElement.parentElement.parentElement.parentElement.children[1].firstElementChild.style.display = "none"
            e.target.parentElement.parentElement.parentElement.parentElement.children[1].firstElementChild.firstElementChild.firstElementChild.value = ''
            break;
        case "text_fields":
            e.target.parentElement.parentElement.parentElement.nextElementSibling.nextElementSibling.firstElementChild.style.display = "block"
            e.target.parentElement.previousElementSibling.firstElementChild.innerHTML = "text_fields"
            e.target.parentElement.parentElement.parentElement.nextElementSibling.firstElementChild.style.display = "flex"
            e.target.parentElement.parentElement.parentElement.nextElementSibling.children[1].style.display = "none"
            if (e.target.parentElement.parentElement.parentElement.nextElementSibling.children[2]) e.target.parentElement.parentElement.parentElement.nextElementSibling.children[2].remove()
            e.target.parentElement.parentElement.parentElement.parentElement.children[1].firstElementChild.style.display = "block"
    }
}
function getposition(element) {
    var position = 0
    var numchilds = document.getElementById('listamedias').children.length
    var i = 0;
    while ((element = element.previousElementSibling) != null) ++i;
    return i;
}
function excluir(e) {
    var position = getposition(e.target.parentElement.parentElement.parentElement)
    e.target.parentElement.parentElement.parentElement.remove()
    arquivos.splice(position, 1)
}
ipcRenderer.on('Enviados', (event, arg) => {
    var tableenviados = document.getElementById("tabelaenviaos");
    var row = tableenviados.insertRow(-1);
    var cell1 = row.insertCell(0);
    cell1.innerHTML = arg;
    var tablelista = document.getElementById("tabelamandar")
    tablelista.deleteRow(1)
})
ipcRenderer.on('getPreviousList-reply', (event, arg) => {
    var fileUpload = document.getElementById("fileUpload");
    fileUpload.value = ''
    var table = document.createElement("table");
    table.classList.add('table');
    table.classList.add('table-striped');
    table.classList.add('bg-white');
    table.id = "tabelamandar"
    var rows = arg

    for (var i = 0; i < rows.length; i++) {
        var cells = rows[i].split(";");

        if (cells.length >= 1) {
            var row = table.insertRow(-1);
            for (var j = 0; j < cells.length; j++) {
                var cell = row.insertCell(-1);
                cell.innerHTML = cells[j];
            }
        }
    }
    var dvCSV = document.getElementById("dvCSV");
    dvCSV.innerHTML = "";
    document.getElementById('enviados').innerHTML = "";
    dvCSV.appendChild(table);
    header = table.createTHead();
    row = header.insertRow(0)
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    cell1.innerHTML = "Numero";
    cell2.innerHTML = "Nome";
    //document.getElementById("labelfileUpload").innerText = 'Escolha um arquivo'
 
})
function salvarListaAtual() {
    var numerosarray = document.querySelectorAll('table tbody td:first-child')
    var nomesarray = document.querySelectorAll('table tbody td:nth-child(2)')
    var numeros = []
    var nomes = []
    numerosarray.forEach(function (numero) {
        numeros.push(numero.innerHTML)
    })
    nomesarray.forEach(function (nome) {
        console.log(typeof nome)
        nomes.push(nome.innerHTML)
    })
    ipcRenderer.send('saveNumber', numeros)
    ipcRenderer.send('saveNames', nomes)

    alert("Lista Salva");
}
ipcRenderer.on('Salvar', (event, arg) => {
    var numerosarray = document.querySelectorAll('table tbody td:first-child')
    var nomesarray = document.querySelectorAll('table tbody td:nth-child(2)')
    var numeros = []
    var nomes = []
    numerosarray.forEach(function (numero) {
        numeros.push(numero.innerHTML)
    })
    nomesarray.forEach(function (nome) {
        console.log(typeof nome)
        nomes.push(nome.innerHTML)
    })
    ipcRenderer.send('saveNumber', numeros)
    ipcRenderer.send('saveNames', nomes)
})

ipcRenderer.on('EndProgramAlert', (event, arg) => {

    alert(`Enivados com sucesso: ${arg.sucesso}\n\nEnviados com falha: ${arg.falhas}\n\nTotal de mensagens enviadas: ${arg.total}`)
})